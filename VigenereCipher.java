import java.util.Scanner;

public class java {
    public static void main(String[] args) {
        Scanner d = new Scanner(System.in);
        System.out.println("Enter the Plain text and the KEY");
        String plain = d.nextLine();
        String key = d.nextLine();
        
        String cipher = vigenere(plain, key, false);
        System.out.println("Encryption is " + cipher);
        System.out.println("Decryption is " + vigenere(cipher, key, true));
    }
    
    private static String vigenere(String text, String key, boolean isEncrypted) {
        int i, j = 0;
        int a, b;
        StringBuilder answer = new StringBuilder();
        
        for(i = 0; i < text.length(); i++) {
            a = text.charAt(i) - 'a';
            b = key.charAt(j) - 'a';
            if(!isEncrypted)
                answer.append((char)(((a + b) % 26) + 'a'));
            else {
                answer.append((char)(((a - b + 26) % 26) + 'a'));
            }
            
            j = (j + 1) % key.length();
        }
        return answer.toString();
    }
}
