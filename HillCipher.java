/* Process
INPUT
    1. Get key matrix length
    2. Get key matrix
    3. Get plain text
    
IMPLEMENTATIONS
    Formula
        c = key * p % 26
    1. Create n X 1 matrix and assign values 
        1.1 If no values available assign 'x'
    keyMatrix * PlainMatrix % 26
    Append value

METHODS
    encrypt(plain, key[][])
*/

import java.util.Scanner;

public class Cryptography {

    public static void main(String[] args) {
        Scanner d = new Scanner(System.in);
        
        // Inputs 
        System.out.println("Enter the limits for Key matrix");
        int n = d.nextInt();
        System.out.println("Enter matrix elements");
        int i, j;
        int key[][] = new int[n][n];
        for(i = 0; i < n; i++) {
            for(j = 0; j < n; j++) {
                key[i][j] = d.nextInt();
            }
        }
        System.out.println("Enter the plain text");
        d.nextLine();
        String plain = d.nextLine();
        System.out.println(encrypt(key, plain));
    }
    
    private static String encrypt(int[][] key, String plainText) {
        int i, j, ptr = 0;
        int n = key.length;
        int plain[][] = new int[n][1];
        
        StringBuilder text = new StringBuilder();
        StringBuilder answer = new StringBuilder();
        for(i = 0; i < plainText.length(); i++) {
            if(Character.isAlphabetic(plainText.charAt(i)))  text.append(plainText.charAt(i));
        }
        
        while(ptr < text.length()) {
            for(i = 0; i < n; i++) {
                if(ptr < text.length()) plain[i][0] = text.charAt(ptr++) - 'a';
                else plain[i][0] = 'x';
            }
            
            // Matrix multiplication
            int[][] multiply = matrixMultiply(key, plain);
            
            for(i = 0; i < multiply.length; i++) {
                for(j = 0; j < multiply[i].length; j++) {
                    System.out.println((char) (multiply[i][j] % 26 + 'a'));
                    answer.append((char)(multiply[i][j] % 26 + 'a'));
                }
            }
        }
        
        
        return answer.toString();
    }
    
    private static int[][] matrixMultiply(int[][] key, int[][] plain) {
        int i, j, k, l;
        int a[][] = new int[key.length][plain[0].length];
        for(i = 0; i < a.length; i++) {
            for(j = 0; j < a[i].length; j++) {
                for(k = 0; k < plain.length; k++) {
                    a[i][j] += key[i][k] * plain[k][j];
                }
            }
        }
        return a;
    }
}
