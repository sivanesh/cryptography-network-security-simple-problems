import java.util.Scanner;

public class Base {
    
    public static void main(String[] args) {
        Scanner d = new Scanner(System.in);
        
        // Inputs
        System.out.println("Enter String and key value");
        String s = d.nextLine().toLowerCase();
        int key = d.nextInt();
        
        // Implementations
        String cipher = caesar(s, key);
        System.out.println("Cipher text = " + cipher);
        System.out.println("Normal text = " + caesar(cipher, -key));
    }
    
    private static String caesar(String s, int key) {
        StringBuilder cipher = new StringBuilder();
        char c;
        int i, ch;
        for(i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            if(Character.isAlphabetic(c)) {
                ch = ((c - 'a') + key) % 26;
                
                // For decryption ie -key value
                if(ch < 0) {
                    ch = 25 - ch - 1;
                }
                cipher.append((char)(ch + 'a'));
            }
        }
        return cipher.toString();
    }
    
}
