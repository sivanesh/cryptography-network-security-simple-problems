import java.util.Scanner;

public class RailFenceCipher {
    
    static char[][] matrix;
    
    public static void main(String[] args) {
        Scanner d = new Scanner(System.in);
        System.out.println("Enter key length(number) and Plain text");
        int n = d.nextInt();
        d.nextLine();
        String plain = d.nextLine();
        matrix = new char[n][plain.length() / n];
        generateMatrix(n, plain);
        String cipher = encrypt();
        System.out.println("\nCipher text is " + cipher);

    }
    
    static private void generateMatrix(int n, String plain) {
        int i, j, ptr = 0;
        
        for(j = 0; j < matrix[0].length; j++) {
            for(i = 0; i < matrix.length; i++) {
                if(plain.length() > ptr) {
                while(plain.charAt(ptr) < 'a' || plain.charAt(ptr) > 'z')
                    ptr++;
                
                    matrix[i][j] = plain.charAt(ptr++);
                }
                else matrix[i][j] = 'x';
            }
        }
        
        // Matrix printer
        for(i = 0; i < matrix.length; i++) {
            System.out.println("");
            for(j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
        }
    }
    
    private static String encrypt() {
        StringBuilder s = new StringBuilder();
        int i, j;
        
        for(i = 0; i < matrix.length; i++) 
            for(j = 0; j < matrix[0].length; j++) s.append(matrix[i][j]);
            
        return s.toString();
    }
}
